/**
 * 公用程式
 */

function clog(o) {
	console.log(o);
}
function clogs(s, o) {
	clog(s);
	clog(o);
}
function clog_red(str){
	console.log('%c'+str, 'background: #FFFF33; color: red');
}
function jlog(s){
    $("#log").append("<br/>->" + s);
}

function sleep(numberMillis) { 
   var now = new Date();
   var exitTime = now.getTime() + numberMillis;  
   while (true) { 
       now = new Date(); 
       if (now.getTime() > exitTime)    return;
    }
}

function isJSON(obj){
    var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length;    
    return isjson;
}

function lengthJO(jo){
	var count = 0;
	for(var key in jo){
		count++;
	}
	return count;
}

//取得一個ja
function jo_sortByValue(jo){
    var sortable = [];
    for (var vehicle in jo) {
        sortable.push([vehicle, jo[vehicle]]);
    }

    sortable.sort(function(a, b) {
        return a[1] - b[1];
    });

    return sortable;
}

//逃逸html特殊字元，用以完整顯示內容
function escapeHtml(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/\n/g, "<br/>")
    	.replace(/ /g, "&nbsp;");
}

//計算字串的「字元」長度（中文=2）
function bLength(s){
	var arr = s.match(/[^\x00-\xff]/ig); 
	return (arr == null ? s.length : s.length + arr.length);
}

function jaHasElement(ja, s){
	var bHas = false;
	for(var ija in ja){
		if(ja[ija] == s){
			bHas = true;
			break;
		}
	}
	return bHas;
}

function replaceAll(replaced, replaceFrom, replaceTo){
    while(replaced.indexOf(replaceFrom) > -1){
        replaced = replaced.replace(replaceFrom, replaceTo);
    }
    return replaced;
}

//反轉jo(key,value反轉)
function reverseJO(jo){
  var newJo = {};
  for(var key in jo){
      newJo[jo[key]] = key;
  }
  return newJo;
}

//補零（digit=需補足的位數；EX：balanceZero("6", 3) = 006）
function balanceZero(s, digit){
	s = "" + s;
	while(s.length < digit){
		s = "0" + s;
	}
	return s;
}

//取得URL上目標參數，未正確獲取回傳null
function getUrlParameter(targetP){
	var result = null;

	var p = location.search.substring(1);
	var ap = p.split("&");
	for(var iap in ap){
		var oneP = ap[iap];
		var aOneP = oneP.split("=");
		var k = aOneP[0];
		var v = aOneP[1];
		// clog("k=>" + k + " v=>" + v);

		if(k == targetP){
			result = v;
		}else{
			//你用了什麼怪參數名？
		}
	}

	return result;
}

//取得瀏覽器高度
function getBrowserHeight() {
	return $(window).height();
}
//取得瀏覽器寬度
function getBrowserWidth() {
	return $(window).width();
}

//必須呼叫於document.ready，要處理成相同最大高度的元素需使用同個className
//paddingBotton = 內容底邊留白，不處理可填0
//EX：setMaxHeight("contentDiv", 15);
function setMaxHeight(className, paddingBotton){
	var maxH = 0;
	for(var i=0; i<$("."+className).length; i++){
		var getH = $($("."+className)[i]).height();
		if(maxH < getH){	maxH = getH;	}
	}
	maxH = maxH + parseInt(paddingBotton);
	$("."+className).css('height', maxH);
}

function getServerIp(){
	var str = document.location.href;
	var x = str.indexOf("/", 7);	// 略過 "http:// , 然後抓到第一郭 '/' 為止 , 這樣就應該是 IP 了 , 至少是 FULL DNS NAME
	if (x == -1) x = str.length;
	Server_IP = str.substring(7,x);
	return Server_IP;
}

//判斷檔案是否存在
function isFileExist(url){
	var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    if(http.status != 404){
      return true;
    }else{
      return false;
    }
}

//檢查mail格式
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

//檢查手機格式
function validatePhonenumber(phone) {  
  var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
  return phoneno.test(phone);
}  

//取得存es的timestamp
//傳入「Date」，如當前時間「new Date()」。EX: getEsTimestamp(new Date());
function getEsTimestamp(a) {
	var b = a.getHours();
    return a.setHours(b + 8), a = a.toJSON(), a = a.replace("Z", "+08:00")
}

//取得指定月份的最後一日
function getLastDateOfMonth(year, month){
	var date = "";
	var d = new Date(year, month, 1);	//「月份」在「Date」中要「-1」，因為要算下一個月所以「+1」（抵銷）
	d.setDate(d.getDate()-1);
	return d.getDate();
}

//自動雙引（掠過「英文大寫boolean、符號boolean」
function qParser_addDoubleQuotes(q){
	var newQ = "";
	
	var aQ = q.split(" ");
	for(var iaQ in aQ){
		var oneQ = aQ[iaQ];
		
		if(oneQ.length==0)	continue;	//略過「空值」
		
		//「布林」不處理
		if(oneQ!="AND" && oneQ!="OR" && oneQ!="NOT"){
			//保留的「符號boolean」
			var booleanString = "";
			if(oneQ.startsWith("+") || oneQ.startsWith("-")){
				booleanString = oneQ.substring(0, 1);
				
				booleanString = booleanString.replace();
				oneQ = oneQ.substring(1, oneQ.length);
			}
//			clog("booleanString=>" + booleanString);
			
			if(!oneQ.startsWith("\"") && !oneQ.endsWith("\""))	oneQ = "\"" + oneQ + "\"";
			
			oneQ = booleanString + oneQ
		}
		
		if(newQ != "")	newQ += " ";
		newQ += oneQ;
	}
	
	return newQ;
}

//string對「符號」加「/」
function sParser_addSlashToSymbol(s){
	//!!!處理特殊符號時，要注意「/」是無法處理的...於s取得時就必須是「//」
	var ns = "";
	for(var i = 0; i < s.length; i++) {
		if(new RegExp("[\\d\\w]", "g").test(s.substring(i, i+1)) || s.charCodeAt(i)>0x4E00 && s.charCodeAt(i)<0x9FA5) {
			ns += s.substring(i, i+1);
		}else{
			ns += "/" + s.substring(i, i+1);
		}
	}
	return ns;
}
//string對「符號」加「/」，避開lucene查詢字元，感覺多餘了...
function sParser_addSlashToSymbol(s){
	//!!!處理特殊符號時，要注意「/」是無法處理的...於s取得時就必須是「//」
	var ns = "";
	for(var i = 0; i < s.length; i++) {
		if(new RegExp("[\\d\\w]", "g").test(s.substring(i, i+1)) || s.charCodeAt(i)>0x4E00 && s.charCodeAt(i)<0x9FA5
				|| new RegExp("[+-|!^~*?:\/]", "g").test(s.substring(i, i+1))
				|| s.substring(i, i+1)=="(" || s.substring(i, i+1)==")" || s.substring(i, i+1)=="{" || s.substring(i, i+1)=="}"
				|| s.substring(i, i+1)=="[" || s.substring(i, i+1)=="]" || s.substring(i, i+1)=="&"
				|| s.substring(i, i+1)==" ") {
			ns += s.substring(i, i+1);
		}else{
			ns += "/" + s.substring(i, i+1);
		}
	}
	return ns;
}